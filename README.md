# Spotilife
![Home photo](/assets/home-img.jpg)

## Description
This project is built first of all to practice the scalable file structure to make the project easier to maintain in the future. Also this application is built to see another way to view spotify data with simple fronted.

## Link
Website link: [Spotilife](https://ae34325e.spotilife.pages.dev/#/)

## Technologies 
This project is built with:
1. [Angular](https://angular.io/)
2. [NodeJS](https://nodejs.org/es/)
3. [Express](https://expressjs.com/es/)
4. [Cloudflare](https://dash.cloudflare.com/) for frontend 
5. [Render](https://render.com/) for backend

## References
1. [How to define a hight structure in Angular](https://itnext.io/choosing-a-highly-scalable-folder-structure-in-angular-d987de65ec7)
2. [Antonio Perez (Senior Angular developer)](https://www.youtube.com/user/apcano1978)
3. [Decode Frontend](https://www.youtube.com/c/DecodedFrontend)
4. [Akotech](https://www.youtube.com/channel/UCmUan2ua5awsm_qiHNMYf7Q)

## Visuals
### Mini demo
If you go to the web site it looks much sharper and without the dots. 

![home gif](/assets/home-gif.gif)

## Setup
```bash
    cd existing_repo
    git remote add origin https://gitlab.com/Bangal22/spotiprofile.git
    git branch -M main
    git push -uf origin main
```
