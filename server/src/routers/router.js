import express from 'express';
import { 
    login, 
    callback, 
    refresh_token,
} from '../controller/index.js';

export const router = express.Router();

router.get('/login', login);
router.get('/callback', callback);
router.get('/refresh_token/:refresh_token', refresh_token);



