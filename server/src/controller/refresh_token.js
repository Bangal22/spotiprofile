import request from 'request';

export const refresh_token = (req, res) => {
    const { params } = req;
    const { refresh_token } = params;
    
    var authOptions = {
        url: 'https://accounts.spotify.com/api/token',
        form: {
            grant_type: 'refresh_token',
            refresh_token: refresh_token
        },
        headers: {
            'Authorization': 'Basic ' + Buffer.from(process.env.CLIENT_ID + ':' + process.env.CLIENT_SECRET).toString('base64')
        },
        json: true
    }

    request.post(authOptions, (error, response, body) => {
        const { statusCode } = response;
        if (!error && statusCode === 200) {
            const { access_token, refresh_token } = body;
            return res.status(statusCode).json({ access_token, refresh_token })
        }
        return res.status(statusCode)
    });
}