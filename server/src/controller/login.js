import {generateRandomString} from '../utils/generateRandomString.js'

export const login = (req, res) => {
    const state = generateRandomString(16);
    const scope = 'user-read-private user-read-email user-read-recently-played user-top-read user-follow-read user-follow-modify playlist-read-private playlist-read-collaborative playlist-modify-public';
    const params = new URLSearchParams({
        response_type: 'code',
        client_id : process.env.CLIENT_ID,
        scope,
        redirect_uri : process.env.REDIRECT_URI,
        state,
    });
    res.status(200).send({url: 'https://accounts.spotify.com/authorize?' + params})
}