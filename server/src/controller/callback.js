import request from 'request';

export const callback = (req, res) => {
    const { query } = req;
    const { code, state } = query;

    if (state === null) res.redirect('/' + new URLSearchParams({ error: 'state_mismatch' }))

    var authOptions = {
        url: `${process.env.URL}/token`,
        form: {
            code: code,
            redirect_uri: process.env.REDIRECT_URI,
            grant_type: 'authorization_code'
        },
        headers: {
            'Authorization': 'Basic ' + Buffer.from(process.env.CLIENT_ID + ':' + process.env.CLIENT_SECRET).toString('base64')
        },
        json: true
    }

    request.post(authOptions, (error, response, body) => {
        const { statusCode } = response;

        if (!error && statusCode === 200) {

            const { access_token, refresh_token } = body;
            const params = new URLSearchParams({ 
                access_token : encodeURIComponent(access_token), 
                refresh_token : encodeURIComponent(refresh_token), 
            });
            return res.status(200).redirect(`${process.env.URL_FRONT}/validation?${params}`)
        }

        const params = new URLSearchParams({ error: 'invalid_token' })
        return res.status(403).redirect(`${process.env.URL_FRONT}/home?${params}`)
    });}
