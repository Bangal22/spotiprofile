import http from 'http';
import express from 'express';
import cors from 'cors';
import { router } from './routers/router.js';
import path from 'path';
import { fileURLToPath } from 'url'
import * as dotenv from 'dotenv'

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);


const app = express();
const server = http.createServer(app);
dotenv.config();

app.use(cors())
app.use(express.static(__dirname));
app.use(express.json())
app.use('/', router)


server.listen(process.env.PORT || 9000, () => {
    console.log("Listen")
})