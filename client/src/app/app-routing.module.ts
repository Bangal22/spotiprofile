import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/guards/auth.guard';
import { IsLoggedGuard } from './core/guards/is-logged.guard';

const routes: Routes = [
  { path: '', loadChildren: () => import('./modules/login/login.module').then(module => module.LoginModule), canActivate: [IsLoggedGuard] },
  { path: 'validation', loadChildren: () => import('./modules/validation/validation.module').then(module => module.ValidationModule) },
  { path: 'home', loadChildren: () => import('./modules/home/home.module').then(module => module.HomeModule), canActivate: [AuthGuard] },
  { path: 'tracks', loadChildren: () => import('./modules/tracks/tracks.module').then(module => module.TracksModule), canActivate: [AuthGuard] },
  { path: 'artists', loadChildren: () => import('./modules/artists/artists.module').then(module => module.ArtistsModule), canActivate: [AuthGuard] },
  { path: 'playlist/:id', loadChildren: () => import('./modules/playlists/playlists.module').then(module => module.PlaylistsModule), canActivate: [AuthGuard] },
  { path: 'search', loadChildren: () => import('./modules/search/search.module').then(module => module.SearchModule), canActivate: [AuthGuard] },
  { path: 'error404', loadChildren: () => import('./modules/error-page/error-page.module').then(module => module.ErrorPageModule), canActivate: [AuthGuard] },
  { path: '**', loadChildren: () => import('./modules/home/home.module').then(module => module.HomeModule), canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule { }
