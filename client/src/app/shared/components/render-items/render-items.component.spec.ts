import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RenderItemsComponent } from './render-items.component';

describe('RenderItemsComponent', () => {
  let component: RenderItemsComponent;
  let fixture: ComponentFixture<RenderItemsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RenderItemsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RenderItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
