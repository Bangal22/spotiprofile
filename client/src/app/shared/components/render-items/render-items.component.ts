import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-render-items',
  templateUrl: './render-items.component.html',
  styleUrls: ['./render-items.component.scss']
})
export class RenderItemsComponent implements OnInit { 

  @Input('items') items! : any;
  @Input('href') href! :string;
  @Input('title') title! : string;
  @Input ('class') class :string = 'little-border';

  constructor() { }

  ngOnInit(): void {
  }

}
