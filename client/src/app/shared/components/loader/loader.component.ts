import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  hidden(){
    setTimeout(() => {
      const spinnerContainer = document.getElementById('my-overlay');
      if(spinnerContainer){
        spinnerContainer.style.display = "none";
      }
    },100)
  }

  show(){ 
    const spinnerContainer = document.getElementById('my-overlay');
    if(spinnerContainer) spinnerContainer.style.display = "flex";
  }
}
