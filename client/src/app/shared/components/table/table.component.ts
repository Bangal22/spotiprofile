import { Component, Input, OnInit } from '@angular/core';
import { TrackReduced } from 'src/app/interfaces/Tracks';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  @Input() tracks! : TrackReduced[];

  constructor() { }

  ngOnInit(): void {
  }

}
