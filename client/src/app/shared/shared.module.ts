import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MinutesPipe } from './pipes/minutes.pipe';
import { UpperCasePipe } from './pipes/upper-case.pipe';
import { ArtistCardComponent } from './components/artist-card/artist-card.component';
import { WhiteButtonComponent } from './components/white-button/white-button.component';
import { TableComponent } from './components/table/table.component';
import { PlayerComponent } from './components/player/player.component';
import { PlayButtonComponent } from './components/play-button/play-button.component';
import { LoaderComponent } from './components/loader/loader.component';
import { RenderItemsComponent } from './components/render-items/render-items.component';


@NgModule({
  declarations: [
    UpperCasePipe,
    ArtistCardComponent,
    WhiteButtonComponent,
    MinutesPipe,
    TableComponent,
    PlayerComponent,
    PlayButtonComponent,
    PlayButtonComponent,
    LoaderComponent, 
    RenderItemsComponent
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    ArtistCardComponent,
    WhiteButtonComponent,
    UpperCasePipe, 
    MinutesPipe, 
    TableComponent, 
    PlayerComponent, 
    PlayButtonComponent, 
    LoaderComponent, 
    RenderItemsComponent
  ]
})
export class SharedModule { }
