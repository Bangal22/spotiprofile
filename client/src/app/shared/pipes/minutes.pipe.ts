import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'minutes'
})
export class MinutesPipe implements PipeTransform {

  transform(milliseconds: number, ...args: unknown[]): unknown {
    var minutes = Math.floor(milliseconds / 60000);
    var seconds = parseInt(((milliseconds % 60000) / 1000).toFixed(0));
    return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
  }

}
