import { TrackReduced, ExternalIDS, ArtistType, AlbumTypeEnum, ReleaseDatePrecision } from "./Tracks";
import { Image, ExternalUrls, Followers} from './User';
export interface SearchResponse {
 albums:  Albums;
 artists: ArtistsSearch;
 tracks:  Tracks;
}

export interface Albums {
 href:     string;
 items:    AlbumElement[];
 limit:    number;
 next:     string;
 offset:   number;
 previous: null;
 total:    number;
}

export interface AlbumElement {
 album_type:             AlbumTypeEnum;
 artists:                ArtistSearch[];
 available_markets:      string[];
 external_urls:          ExternalUrls;
 href:                   string;
 id:                     string;
 images:                 Image[];
 name:                   string;
 release_date:           string;
 release_date_precision: ReleaseDatePrecision;
 total_tracks:           number;
 type:                   AlbumTypeEnum;
 uri:                    string;
}


export interface ArtistSearch {
 external_urls: ExternalUrls;
 href:          string;
 id:            string;
 name:          string;
 type:          ArtistType;
 uri:           string;
}


export interface ArtistsSearch {
 href:     string;
 items:    ArtistsItem[];
 limit:    number;
 next:     string;
 offset:   number;
 previous: null;
 total:    number;
}

export interface ArtistsItem {
 external_urls: ExternalUrls;
 followers:     Followers;
 genres:        string[];
 id:            string;
 images:        Image[];
 name:          string;
 popularity:    number;
 type:          ArtistType;
 uri:           string;
}

export interface Tracks {
 href:     string;
 items:    TracksItem[];
 limit:    number;
 next:     string;
 offset:   number;
 previous: null;
 total:    number;
}

export interface TracksItem {
 album:             AlbumElement;
 artists:           ArtistSearch[];
 available_markets: string[];
 disc_number:       number;
 duration_ms:       number;
 explicit:          boolean;
 external_ids:      ExternalIDS;
 external_urls:     ExternalUrls;
 href:              string;
 id:                string;
 is_local:          boolean;
 name:              string;
 popularity:        number;
 preview_url:       string;
 track_number:      number;
 type:              PurpleType;
 uri:               string;
 images:            Image[], 
}

export enum PurpleType {
 Track = "track",
}

/* ****** */

export interface SearchItems{
    albums: AlbumItems[],
    artist : ArtistItems[], 
    tracks : TrackReduced[], 
}
export interface ArtistItems {
    id: string,
    name : string, 
    type : string, 
    images : Image
    url : string,
}

export interface AlbumItems {
    id: string,
    name : string, 
    type : string, 
    images : Image
}