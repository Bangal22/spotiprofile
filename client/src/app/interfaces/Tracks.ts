import { ExternalUrls, Image } from './User';

export interface TracksResponse {
    href: string;
    items: Track[];
    limit: number;
    next: null;
    offset: number;
    previous: null;
    total: number;
}

export interface Track {
    album: AlbumTracks;
    artists: ArtistTracks[];
    available_markets: string[];
    disc_number: number;
    duration_ms: number;
    explicit: boolean;
    external_ids: ExternalIDS;
    external_urls: ExternalUrls;
    href: string;
    id: string;
    is_local: boolean;
    name: string;
    popularity: number;
    preview_url: null | string;
    track_number: number;
    type: ItemTypeTracks;
    uri: string;
    image: Image
}

export interface AlbumTracks {
    album_type: AlbumType;
    artists: ArtistTracks[];
    available_markets: string[];
    external_urls: ExternalUrls;
    href: string;
    id: string;
    images: Image[];
    name: string;
    release_date: string;
    release_date_precision: ReleaseDatePrecision;
    total_tracks: number;
    type: AlbumTypeEnum;
    uri: string;
}

export enum AlbumType {
    Album = "ALBUM",
    Single = "SINGLE",
}

export interface ArtistTracks {
    external_urls: ExternalUrls;
    href: string;
    id: string;
    name: string;
    type: ArtistType;
    uri: string;
}


export enum ArtistType {
    Artist = "artist",
}


export enum ReleaseDatePrecision {
    Day = "day",
    Year = "year",
}

export enum AlbumTypeEnum {
    Album = "album",
    Compilation = "compilation",
    Single = "single",
}

export interface ExternalIDS {
    isrc: string;
}

export enum ItemTypeTracks {
    Track = "track",
}

export interface TrackReduced {
    id: string,
    name: string,
    artists: ArtistTracks[],
    images: Image,
    duration_ms: number;
    album_name: string;
    url: string;
}
