import { Artist } from './Artist';
import { ExternalUrls, Image } from './User';
import { Track, ReleaseDatePrecision, AlbumTypeEnum } from './Tracks';

export interface TopTracksResponse {
    tracks: Track[];
}

export interface AlbumTopTracks {
    album_type: AlbumTypeEnum;
    artists: Artist[];
    external_urls: ExternalUrls;
    href: string;
    id: string;
    images: Image[];
    name: string;
    release_date: Date;
    release_date_precision: ReleaseDatePrecision;
    total_tracks: number;
    type: AlbumTypeEnum;
    uri: string;
}


export enum TrackType {
    Track = "track",
}
