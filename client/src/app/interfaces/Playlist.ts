import { Image, Followers, ExternalUrls } from './User';
import { ExternalIDS, AlbumTypeEnum, ReleaseDatePrecision } from './Tracks';
import { TrackType } from './Top-Tracks';

export interface PlaylistResponse {
    collaborative: boolean;
    description: string;
    external_urls: ExternalUrls;
    followers: Followers;
    href: string;
    id: string;
    images: Image[];
    name: string;
    owner: OwnerPlaylist;
    primary_color: string;
    public: boolean;
    snapshot_id: string;
    tracks: TracksPlaylist;
    type: string;
    uri: string;
}

export interface OwnerPlaylist {
    display_name?: string;
    external_urls: ExternalUrls;
    href: string;
    id: string;
    name?: string;
    type: OwnerTypePlaylist;
    uri: string;
}

export enum OwnerTypePlaylist {
    Artist = "artist",
    User = "user",
}

export interface TracksPlaylist {
    href: string;
    items: ItemPlaylist[];
    limit: number;
    next: string;
    offset: number;
    previous: null;
    total: number;
}

export interface ItemPlaylist {
    added_at: Date;
    added_by: OwnerPlaylist;
    is_local: boolean;
    primary_color: string | null;
    track: TrackPlaylist;
    video_thumbnail: VideoThumbnail;
}

export interface TrackPlaylist {
    album: AlbumPlaylist;
    artists: OwnerPlaylist[];
    available_markets: string[];
    disc_number: number;
    duration_ms: number;
    episode: boolean;
    explicit: boolean;
    external_ids: ExternalIDS;
    external_urls: ExternalUrls;
    href: string;
    id: string;
    is_local: boolean;
    name: string;
    popularity: number;
    preview_url: null | string;
    track: boolean;
    track_number: number;
    type: TrackType;
    uri: string;
}

export interface AlbumPlaylist {
    album_type: AlbumTypeEnum;
    artists: OwnerPlaylist[];
    available_markets: string[];
    external_urls: ExternalUrls;
    href: string;
    id: string;
    images: Image[];
    name: string;
    release_date: string;
    release_date_precision: ReleaseDatePrecision;
    total_tracks: number;
    type: AlbumTypeEnum;
    uri: string;
}

export interface VideoThumbnail {
    url: null;
}

export interface PlaylistReduced {
    name : string,
    description : string,
    external_urls: string,
    followers: number,
    images : Image[],
    owner : OwnerPlaylist,
    tracks: any,
    primary_color: string,
    total: number,
}