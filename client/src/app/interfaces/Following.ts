import { Followers, ExternalUrls, Image } from "./User";
import { Item } from "./Playlists";

export interface FollowingResponse {
    artists: Artists;
}
export interface Artists {
    items:   Item[];
    next:    null;
    total:   number;
    cursors: Cursors;
    limit:   number;
    href:    string;
}

export interface Cursors {
    after: null;
}

export interface TotalFollowing {
    total : number
}
