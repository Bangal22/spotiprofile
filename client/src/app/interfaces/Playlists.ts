import { ExternalUrls, Image } from "./User";

export interface PlaylistsResponse {
    href:     string;
    items:    Item[];
    limit:    number;
    next:     null;
    offset:   number;
    previous: null;
    total:    number;
}

export interface Item {
    collaborative: boolean;
    description:   string;
    external_urls: ExternalUrls;
    href:          string;
    id:            string;
    images:        Image[];
    name:          string;
    owner:         Owner;
    primary_color: null;
    public:        boolean;
    snapshot_id:   string;
    tracks:        TracksPlayLists;
    type:          ItemTypePLaylist;
    uri:           string;
}


export interface Owner {
    display_name:  string;
    external_urls: ExternalUrls;
    href:          string;
    id:            string;
    type:          OwnerType;
    uri:           string;
}

export enum OwnerType {
    User = "user",
}

export interface TracksPlayLists {
    href:  string;
    total: number;
}

export enum ItemTypePLaylist {
    Playlist = "playlist",
}

export interface TotalPlaylist {
    total : number
}
