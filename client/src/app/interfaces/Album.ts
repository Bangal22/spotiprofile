import {ExternalUrls, Image} from './User';

export interface AlbumsResponse {
    albums : AlbumResponse
}
export interface AlbumResponse {
 href:     string;
 items:    Album[];
 limit:    number;
 next:     string;
 offset:   number;
 previous: null;
 total:    number;
}

export interface Album {
 album_group:            string;
 album_type:             string;
 artists:                ArtistAlbum[];
 available_markets:      string[];
 external_urls:          ExternalUrls;
 href:                   string;
 id:                     string;
 images:                 Image[];
 name:                   string;
 release_date:           Date;
 release_date_precision: string;
 total_tracks:           number;
 type:                   string;
 uri:                    string;
}

export interface ArtistAlbum {
 external_urls: ExternalUrls;
 href:          string;
 id:            string;
 name:          string;
 type:          string;
 uri:           string;
}

export interface AlbumReduced {
    url: string, 
    images : Image, 
    name : string, 
    type : string
    external_url : string,
}

export interface Releases {
    url : string, 
    images : Image, 
    name : string, 
    type : string,
    id : string
}