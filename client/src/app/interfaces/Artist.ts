import {Followers, ExternalUrls, Image} from './User';

export interface ArtistResponse {
    href: string;
    items: Artist[];
    limit: number;
    next: null;
    offset: number;
    previous: null;
    total: number;
}

export interface Artist {
    external_urls: ExternalUrls;
    followers: Followers;
    genres: string[];
    // href: string;
    id: string;
    images: Image[];
    name: string;
    popularity: number;
    type: Type;
    uri: string;
}

export enum Type {
    Artist = "artist",
}

export interface ArtistReduced {
    name : string, 
    type : string,
    images : Image, 
    id : string, 
    url: string, 
    external_url : string
}
