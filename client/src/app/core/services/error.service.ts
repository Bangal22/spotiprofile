import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor(
    private router : Router
  ) { }

  redirectToError401 (error:string) {
    console.error(error);
    this.router.navigate(['/error404'])
  }
}
