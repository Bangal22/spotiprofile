import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TokenResponse } from 'src/app/interfaces';

@Injectable({
  providedIn: 'root'
})
export class JwtService {

  constructor() { }

  /**
   * It returns an observable that emits the value of the access_token stored in sessionStorage.
   * @returns A BehaviorSubject that is an Observable.
   */
  getToken() {
    return sessionStorage.getItem('access_token');
  }

  /**
   * It returns the value of the refresh_token key in sessionStorage.
   * @returns The refresh token is being returned.
   */
  getRefreshToken() {
    return sessionStorage.getItem('refresh_token');
  }


  /**
   * It takes a string as an argument and stores it in the sessionStorage object
   * @param {string} access_token - The access token we got from the server
   */
  updateAccessToken(access_token:string) {
    sessionStorage.setItem('access_token', access_token);
  }

  /**
   * If the access_token exists in sessionStorage, return a new BehaviorSubject with a value of true,
   * otherwise return a new BehaviorSubject with a value of false.
   * @returns A BehaviorSubject that is an Observable.
   */
  tokenExist() {
    if (sessionStorage.getItem('access_token')) {
      return new BehaviorSubject(true).asObservable()
    }
    return new BehaviorSubject(false).asObservable()
  }

  /**
   * It takes a TokenResponse object as an argument, and then it saves the access_token and refresh_token
   * properties of that object to sessionStorage.
   * @param {TokenResponse} tokens - TokenResponse
   */
  saveTokens(tokens: TokenResponse) {
    const { access_token, refresh_token } = tokens;
    sessionStorage.setItem('access_token', access_token);
    sessionStorage.setItem('refresh_token', refresh_token);
  }

  /**
   * It removes the access_token and refresh_token from the sessionStorage.
   */
  destroyToken() {
    sessionStorage.removeItem('access_token')
    sessionStorage.removeItem('refresh_token');
  }

}
