import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HandleErrorService {

  constructor(
  ) { }

  /**
   * If the error is an instance of ErrorEvent, then the error message is the error.error.message.
   * Otherwise, the error message is the error.status and error.message.
   * @param {any} error - any - The error object that was thrown.
   * @returns The error message.
   */
  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) errorMessage = error.error.message;
    else errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    return throwError(() => {
      return errorMessage;
    });
  }
}
