import { Injectable } from '@angular/core';
import { shuffle } from 'lodash';
import { artists } from '../utils';

@Injectable({
  providedIn: 'root'
})
export class ArtistService {

  constructor() { }

  /**
   * It returns a random artist from the array of artists, or the first artist if the array is empty.
   * @returns The first element of the array.
   */
  gerRandomArtist() {
    return shuffle(artists).shift() || artists[0];
  }
}
