import { Injectable } from '@angular/core';
import { shuffle } from 'lodash';
import { colors } from '../utils';

@Injectable({
  providedIn: 'root'
})
export class ColorService {

  color!: string

  constructor() { }


  private getColor() {
    return this.color;
  }

  /**
   * This function takes a string as an argument and sets the color property of the class to that
   * string.
   * @param {string} color - string - The color of the car
   */
  private setColor(color: string) {
    this.color = color;
  }

  /**
   * It returns a random color from the colors array.
   * @returns The color of the current player.
   */
  getBackgroundColor() {
    if (!this.color) {
      const c = shuffle(colors).shift() || colors.purple;
      this.setColor(c)
    }
    return this.getColor();
  }
  
  /**
   * It returns a random color from the colors array, or purple if the colors array is empty
   * @returns The first item in the array.
   */
  gerRandomColor() {
    return shuffle(colors).shift() || colors.purple;
  }


}
