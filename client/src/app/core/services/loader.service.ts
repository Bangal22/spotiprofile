import { Injectable } from '@angular/core';
import { LoaderComponent } from 'src/app/shared/components/loader/loader.component';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  constructor(

  ) { }


  /**
   * The function `show()` is a member of the class `LoaderComponent` and it calls the function `show()`
   * which is a member of the class `LoaderComponent`.
   */
  show() {
    new LoaderComponent().show();
  }

  /**
   * It's a function that hides a loader component.
   */
  hidden() {
    new LoaderComponent().hidden();
  }
}
