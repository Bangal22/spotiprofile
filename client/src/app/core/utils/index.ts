
/**
 * It takes the URL of the current page, replaces the hash with a slash, creates a new URL object, gets
 * the search params, decodes the params, and returns an object with the params.
 * @returns An object with the access_token, refresh_token, and error.
 */
export const getHashParams = () => {
    const url = window.location.href.replace('#', '/')
    const urlParams = new URL(url).searchParams;
    const tokens = {
        access_token: decodeURIComponent(urlParams.get('access_token') || ''),
        refresh_token: decodeURIComponent(urlParams.get('refresh_token') || ''),
        error: decodeURIComponent(urlParams.get('error') || '')
    };
    return tokens;
};

export const changeTimeRange = (id: string) => {
    const d = document.querySelectorAll('button');
    d.forEach(n => n.className = n.id == id ? "active" : '');
}

export const setTimeRange = (timeRange: string) => {
    if (timeRange == 'short_term') return "Last 4 weeks";
    if (timeRange == 'medium_term') return "Last 6 months";
    return "All time";
}

/* It's creating an object with the colors. */
export const colors = {
    indigo: '#EF4444',
    blue: '#F59E0B',
    green: '#10B981',
    red: '#3B82F6',
    yellow: '#6366F1',
    pink: '#8B5CF6',
    purple: '#EC4899',
}

/* It's creating an array of artists. */
export const artists = [
    "Michael Jackson",
    "BB King",
    "Bob Marley",
    "Freddie Mercury",
    "Kurt Cobain",
    "Frank Sinatra",
    "John Lennon",
    "Shakira",
    "Chris Brown",
    "Eminem",
    "DMX",
    "Kendrick Lamar",
    "J Cole",
    "Snoop Dogg",
    "Suka"
];
