import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { JwtService } from '../services';


@Injectable()
export class HttpTokenInterceptor implements HttpInterceptor {

  constructor(
    private jwtService : JwtService,
  ) {}

  /**
   * If the user has a token, add it to the request header
   * @param request - HttpRequest<unknown> - The request object
   * @param {HttpHandler} next - HttpHandler - The next interceptor in the chain.
   * @returns The next.handle(newRequest) is returning an Observable of type HttpEvent.
   */
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const token = this.jwtService.getToken();
    let newRequest = request;
    if( token != null ) {
      newRequest = request.clone({
        headers : request.headers.set('Authorization', 'Bearer ' + token)
      })
    } 
    return next.handle(newRequest)
  }
}
