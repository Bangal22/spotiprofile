import { Component, OnInit } from '@angular/core';
import { PlaylistResponse } from 'src/app/interfaces';
import { PlaylistService } from '../http';
import { ColorService, JwtService } from '../services';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  public color!: string;
  public playlist!: any;

  constructor(
    private colorService: ColorService,
    private playlistService: PlaylistService,
  ) { }

  ngOnInit(): void {
    this.color = `${this.colorService.getBackgroundColor()}b9`;
  }


  /**
   * The function gets the current user's playlists and maps the items to an array of objects with the
   * id, name, and collaborative properties.
   */
  getPlaylist() {
    this.playlistService.getCurrentUserPlaylists().subscribe({
      next: ({ items }) => this.playlist = items.map(({ id, name, collaborative }) => {
        return { id, name, collaborative }
      })
    })
  }

}
