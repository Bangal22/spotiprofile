import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
  ) {
  }
  
  /**
   * It returns an observable of type any.
   * @returns The response from the server.
   */
  spotifyLogin() {
    return this.http.get(`${environment.API_URL}/login`)
  }
  /**
   * It checks if the user is logged in or not.
   * @returns A boolean value.
   */
  loggedIn(): boolean {
    return !!sessionStorage.getItem('access_token');
  }
}
