import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HandleErrorService } from '../services';

@Injectable({
  providedIn: 'root'
})
export class RefreshTokenService {

  constructor(
    private http : HttpClient, 
    private handleErrorService : HandleErrorService, 
  ) { }

  refreshToken(refreshToken : string|null) : Observable<{access_token : string}> {
    return this.http.get<{access_token : string}>(`${environment.API_URL}/refresh_token/${refreshToken}`).pipe(
      catchError(this.handleErrorService.handleError)
    );
  }
}
