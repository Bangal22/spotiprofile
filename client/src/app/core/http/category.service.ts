import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { CategoryItem, CategoryResponse } from 'src/app/interfaces';
import { catchError, map, Observable } from 'rxjs';
import { ColorService, HandleErrorService } from '../services';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(
    private http: HttpClient,
    private handleErrorService: HandleErrorService,
    private colorService: ColorService,
  ) { }


  /**
   * It returns an Observable of CategoryItem[] by making a GET request to the Spotify API, mapping the
   * response to an array of CategoryItem adding the color, and catching any errors.
   * @returns Observable<CategoryItem[]>
   */
  getCategories(): Observable<CategoryItem[]> {
    return this.http.get<CategoryResponse>(`${environment.API_SPOTIFY}/browse/categories?limit=50`).pipe(
      map(({ categories }) => {
        categories.items.forEach(e => e.color = this.colorService.gerRandomColor())
        return categories.items
      }),
      catchError(this.handleErrorService.handleError)
    )
  }
}
