import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Album, AlbumResponse, AlbumsResponse } from 'src/app/interfaces';
import { catchError, map, Observable } from 'rxjs';
import { HandleErrorService } from '../services';

@Injectable({
  providedIn: 'root'
})
export class AlbumService {

  constructor(
    private http: HttpClient,
    private handleErrorService: HandleErrorService
  ) { }


  /**
   * It takes an id and a limit as parameters, and returns an Observable of type Album[]
   * @param {string} id - string - the id of the artist
   * @param {string} limit - The maximum number of items to return. Default: 20. Minimum: 1. Maximum:
   * 50.
   * @returns An Observable of type Album[]
   */
  getAlbumArtist(id: string, limit: string): Observable<Album[]> {
    return this.http.get<AlbumResponse>(`${environment.API_SPOTIFY}/artists/${id}/albums?limit=${limit}`).pipe(
      map(({ items }) => items),
      catchError(this.handleErrorService.handleError)
    )
  }


  /**
   * We're using the HttpClient to make a GET request to the Spotify API, and then we're using the map
   * operator to transform the response into an array of Albums.
   * @returns The getNewReleases() method returns an Observable of type Album[].
   */
  getNewReleases(): Observable<Album[]> {
    return this.http.get<AlbumsResponse>(`${environment.API_SPOTIFY}/browse/new-releases?limit=12`).pipe(
      map(({ albums }) => albums.items),
      catchError(this.handleErrorService.handleError)
    )
  }
}
