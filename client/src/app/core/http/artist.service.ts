import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Artist, ArtistResponse, TopTracksResponse } from 'src/app/interfaces';
import { Track } from 'src/app/interfaces/Tracks';
import { catchError, map, Observable } from 'rxjs';
import { HandleErrorService } from '../services';

@Injectable({
  providedIn: 'root'
})
export class ArtistService {

  constructor(
    private http: HttpClient,
    private handleErrorService: HandleErrorService
  ) { }

  /**
   * It takes in two parameters, limit and time_range, and returns an Observable of type Artist[].
   * @param {string} limit - The number of entities to return. Default: 20. Minimum: 1. Maximum: 50.
   * @param {string} time_range - short_term, medium_term, long_term
   * @returns The response from the API is being returned.
   */
  getArtist(limit: string, time_range: string): Observable<Artist[]> {
    return this.http.get<ArtistResponse>(`${environment.API_SPOTIFY}/me/top/artists?limit=${limit}&time_range=${time_range}`).pipe(
      map(({ items }) => items),
      catchError(this.handleErrorService.handleError)
    )
  }


  /**
   * It takes an id as a parameter, makes a GET request to the Spotify API, and returns an observable of
   * type Artist.
   * @param {string} id - string - the id of the artist you want to retrieve
   * @returns An Observable of type Artist.
   */
  getArtistById(id: string): Observable<Artist> {
    return this.http.get<Artist>(`${environment.API_SPOTIFY}/artists/${id}`).pipe(
      map(res => res),
      catchError(this.handleErrorService.handleError)
    )
  }

  /**
   * It takes an id, makes a request to the Spotify API, and returns an observable of an array of
   * tracks
   * @param {string} id - string
   * @returns The return is an Observable of type Track[]
   */
  getArtistTopTracks(id: string): Observable<Track[]> {
    return this.http.get<TopTracksResponse>(`${environment.API_SPOTIFY}/artists/${id}/top-tracks?market=ES`).pipe(
      map(({ tracks }) => tracks),
      catchError(this.handleErrorService.handleError)
    )
  }
}
