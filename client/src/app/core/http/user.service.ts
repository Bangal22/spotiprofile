import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { FollowingResponse, UserResponse } from 'src/app/interfaces';
import { catchError, Observable } from 'rxjs';
import { HandleErrorService } from '../services/handle-error.service';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient, 
    private handleErrorService : HandleErrorService
  ) { }


  /**
   * The function returns an Observable of type UserResponse
   * @returns Observable<UserResponse>
   */
  getUser(): Observable<UserResponse> {
    return this.http.get<UserResponse>(`${environment.API_SPOTIFY}/me`).pipe(
      catchError(this.handleErrorService.handleError)
    );
  }


  /**
   * It returns an Observable of type FollowingResponse, which is a GET request to the Spotify API
   * endpoint for getting the current user's followed artists
   * @returns An observable of type FollowingResponse
   */
  getFollowings(): Observable<FollowingResponse> {
    return this.http.get<FollowingResponse>(`${environment.API_SPOTIFY}/me/following?type=artist`).pipe(
      catchError(this.handleErrorService.handleError)
    );
  }

}
