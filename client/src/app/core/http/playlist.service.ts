import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PlaylistsResponse, PlaylistResponse } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import { Observable, catchError } from 'rxjs';
import { HandleErrorService } from '../services/handle-error.service';

@Injectable({
  providedIn: 'root'
})
export class PlaylistService {

  constructor(
    private http: HttpClient,
    private handleErrorService: HandleErrorService
  ) { }



  /**
   * It returns an observable of type PlaylistsResponse, which is a class that I created to hold the
   * data that I want to get from the API
   * @returns Observable<PlaylistsResponse>;
   */
  getCurrentUserPlaylists(): Observable<PlaylistsResponse> {
    return this.http.get<PlaylistsResponse>(`${environment.API_SPOTIFY}/me/playlists`).pipe(
      catchError(this.handleErrorService.handleError)
    )
  }

  /**
   * It returns an Observable of type PlaylistResponse, which is a class that I created to hold the
   * data that I want to get from the API
   * @param {string} id - string - the id of the playlist you want to retrieve
   * @returns Observable<PlaylistResponse>;
   */
  getPlayList(id: string): Observable<PlaylistResponse> {
    return this.http.get<PlaylistResponse>(`${environment.API_SPOTIFY}/playlists/${id}`).pipe(
      catchError(this.handleErrorService.handleError)
    )
  }

}
