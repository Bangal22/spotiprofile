import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { SearchResponse } from 'src/app/interfaces';
import { catchError, Observable } from 'rxjs';
import { HandleErrorService } from '../services';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(
    private http: HttpClient,
    private handleErrorService: HandleErrorService
  ) { }


  /**
   * It takes a query, and returns a SearchResponse object.
   * @param {any} query - the search query
   * @returns The getSearchValues() method returns an observable of type SearchResponse.
   */
  getSearchValues(query: any) : Observable<SearchResponse> {
    return this.http.get<SearchResponse>(`${environment.API_SPOTIFY}/search?q=${query}&type=track%2Cartist%2Calbum&limit=12`).pipe(
      catchError(this.handleErrorService.handleError),
    )
  }

}
