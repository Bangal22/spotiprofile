import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable } from 'rxjs';
import { TracksResponse, Track } from 'src/app/interfaces';
import { environment } from 'src/environments/environment';
import { HandleErrorService } from '../services/handle-error.service';


@Injectable({
  providedIn: 'root'
})
export class TracksService {

  constructor(
    private http: HttpClient, 
    private handleErrorService : HandleErrorService
  ) { }

  /**
   * It returns an observable of an array of tracks, which is the response from the Spotify API
   * @param {string} limit - The number of entities to return. Default: 20. Minimum: 1. Maximum: 50.
   * @param {string} time_range - The time range for the chart. Valid values: long_term (calculated
   * from several years of data and including all new data as it becomes available), medium_term
   * (approximately last 6 months), short_term (approximately last 4 weeks). Default: medium_term.
   * @returns The return is an observable of type Track[]
   */
  getTracks(limit:string, time_range : string): Observable<Track[]> {
    return this.http.get<TracksResponse>(`${environment.API_SPOTIFY}/me/top/tracks?limit=${limit}&time_range=${time_range}`)
    .pipe(
      map(({items}) => items),
      catchError(this.handleErrorService.handleError)
    )
  }

  
  
}
