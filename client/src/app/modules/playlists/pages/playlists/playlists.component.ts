import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PlaylistService } from 'src/app/core/http';
import { ColorService } from 'src/app/core/services';
import { ErrorService } from 'src/app/core/services/error.service';
import { LoaderService } from 'src/app/core/services/loader.service';
import { ItemPlaylist, PlaylistReduced } from 'src/app/interfaces';

@Component({
  selector: 'app-playlists',
  templateUrl: './playlists.component.html',
  styleUrls: ['./playlists.component.scss']
})
export class PlaylistsComponent implements OnInit {

  public playlist!: PlaylistReduced;

  constructor(
    private playlistService: PlaylistService,
    private activatedRoute: ActivatedRoute,
    private colorService: ColorService,
    private loaderService: LoaderService,
    private errorService : ErrorService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(({ params }: any) => {
      this.getPlayList(params.id)
    })
  }

  /**
   * I'm using the Spotify API to get a playlist and then I'm mapping the tracks.items array to a new
   * array with the getTracksItems function.
   * 
   * @param {string} id - string - the id of the playlist
   */
  getPlayList(id: string) {
    this.loaderService.show();
    this.playlistService.getPlayList(id).subscribe({
      next: ({ name, description, external_urls, followers, images, owner, tracks }) => this.playlist = {
        name,
        description,
        external_urls: external_urls.spotify,
        followers: followers.total,
        images,
        owner,
        tracks: tracks.items.map(this.getTracksItems),
        primary_color: `${this.colorService.gerRandomColor()}93`,
        total: tracks.total,
      },
      error: error =>  this.errorService.redirectToError401(error),
      complete: () => this.loaderService.hidden()
    })
  }

  /**
   * It takes an object with a property called track, and returns an object with the properties id,
   * name, artists, images, duration_ms, album_name, and url
   * @param {ItemPlaylist}  - ItemPlaylist is the type of the object that is being passed in.
   * @returns An object with the following properties: id, name, artists, images, duration_ms,
   * album_name, url
   */
  getTracksItems({ track }: ItemPlaylist) {
    const { id, name, artists, album, duration_ms, external_urls } = track;
    return { id, name, artists, images: album.images[2], duration_ms, album_name: album.name, url: external_urls.spotify }
  }

}
