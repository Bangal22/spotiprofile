import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/core/http';
import { LoginUrl } from 'src/app/interfaces';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild("loginButton") loginButton! : ElementRef;
  
  public loading = false;
  constructor(
    private authService: AuthService
  ) { }

  ngOnInit(): void {
  }

  /**
   * If the loginUrl is not null, redirect to the loginUrl, otherwise redirect to the error page.
   */
  login() {
    this.loading = true;
    this.loginButton.nativeElement.disabled = true;
    this.loginButton.nativeElement.style.opacity = "0.6";
    this.authService.spotifyLogin().subscribe({
      next: (n: LoginUrl) => window.location.href = n.url || 'http://localhost:4200/#/error-page',
      error : error => console.log(error), 
    })
  }

}
