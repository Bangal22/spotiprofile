import { Component, OnInit } from '@angular/core';
import { ArtistReduced } from 'src/app/interfaces';
import { ArtistService } from 'src/app/core/http';
import { LoaderService } from 'src/app/core/services/loader.service';
import { ErrorService } from 'src/app/core/services/error.service';


@Component({
  selector: 'app-top-artist',
  templateUrl: './top-artist.component.html',
  styleUrls: ['./top-artist.component.scss']
})
export class TopArtistComponent implements OnInit {

  public artists!: ArtistReduced[];

  constructor(
    private artistService: ArtistService,
    private loaderService: LoaderService, 
    private errorService : ErrorService,
  ) { }

  ngOnInit(): void {
    this.getArtist();
  }

  /**
   * We're using the `getArtist` function from the `ArtistService` to get the top artists from the
   * Spotify API. We're then mapping the response to a new array of `ArtistReduced` objects
   */
  getArtist() {
    this.loaderService.show();
    this.artistService.getArtist('6', 'long_term').subscribe({
      next: (response) => this.artists = response.map(({ name, type, images, id, external_urls }): ArtistReduced => {
        return { 
          name, 
          type, 
          images: images[2], 
          id, 
          external_url: external_urls.spotify,
          url: `#/artists/${id}` 
        }
      }),
      error: error => this.errorService.redirectToError401(error),
      complete: () => this.loaderService.hidden()
    })
  }



}
