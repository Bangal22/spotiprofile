import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/interfaces';
import { UserService, PlaylistService } from 'src/app/core/http';
import { LoaderService } from 'src/app/core/services/loader.service';
import { ColorService } from 'src/app/core/services';
import { ErrorService } from 'src/app/core/services/error.service';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {

  public user!: User;
  public followings!: number;
  public playlist!: number;
  public color!: string;

  constructor(
    private userService: UserService,
    private playlistService: PlaylistService,
    private colorService: ColorService,
    private loaderService: LoaderService, 
    private errorService : ErrorService,
  ) { }

  ngOnInit(): void {
    this.color = this.colorService.getBackgroundColor();
    this.getUser();
    this.getFollowings();
    this.getTotalPlayList();
  }

  /**
   * We're calling the getUser() function from the userService, which returns an observable. We're
   * subscribing to that observable, and when the observable emits a value, we're assigning that value
   * to the user property
   */
  getUser() {
    this.loaderService.show();
    this.userService.getUser().subscribe({
      next: ({ display_name, external_urls, followers, images }) => this.user = { display_name, external_urls, followers: followers.total, images: images },
      error: error => this.errorService.redirectToError401(error),
      complete: () => this.loaderService.hidden()
    })
  }


  /**
   * It calls the getFollowings() method from the userService, which returns an observable, and then
   * subscribes to it
   */
  getFollowings() {
    this.loaderService.show();
    this.userService.getFollowings().subscribe({
      next: ({ artists }) => this.followings = artists.total,
      error: error => this.errorService.redirectToError401(error),
      complete: () => this.loaderService.hidden()
    })
  }


  /**
   * It gets the total number of playlists from the Spotify API and assigns it to the playlist
   * variable.
   */
  getTotalPlayList() {
    this.loaderService.show();
    this.playlistService.getCurrentUserPlaylists().subscribe({
      next: ({ total }) => this.playlist = total,
      error: error => this.errorService.redirectToError401(error),
      complete: () => this.loaderService.hidden()
    })
  }



}
