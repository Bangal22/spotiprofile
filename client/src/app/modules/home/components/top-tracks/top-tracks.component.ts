import { Component, OnInit } from '@angular/core';
import { TrackReduced } from 'src/app/interfaces'
import { TracksService } from 'src/app/core/http';
import { LoaderService } from 'src/app/core/services/loader.service';
import { ErrorService } from 'src/app/core/services/error.service';

@Component({
  selector: 'app-top-tracks',
  templateUrl: './top-tracks.component.html',
  styleUrls: ['./top-tracks.component.scss']
})
export class TopTracksComponent implements OnInit {

  public tracks!: TrackReduced[]

  constructor(
    private tracksService: TracksService,
    private loaderService: LoaderService, 
    private errorService : ErrorService,
  ) { }

  ngOnInit(): void {
    this.getTracksLong()
  }


  /**
   * I'm using the Spotify API to get the top 50 tracks of the user, and I'm mapping the response to a
   * new object with the properties I need
   */
  getTracksLong() {
    this.loaderService.show();
    this.tracksService.getTracks('5', 'long_term').subscribe({
      next: response => this.tracks = response.map(({ id, name, artists, album, duration_ms, external_urls }): TrackReduced => {
        return {
          id,
          name,
          artists,
          images: album.images[2],
          duration_ms,
          album_name: album.name,
          url: external_urls.spotify
        }
      }),
      error: error => this.errorService.redirectToError401(error),
      complete: () => this.loaderService.hidden()
    })
  }

}
