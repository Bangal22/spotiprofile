import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './pages/home/home.component';
import { UserInfoComponent } from './components/user-info/user-info.component';
import { TopArtistComponent } from './components/top-artist/top-artist.component';
import { TopTracksComponent } from './components/top-tracks/top-tracks.component';

import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    HomeComponent,
    UserInfoComponent,
    TopArtistComponent,
    TopTracksComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule, 
    SharedModule
  ]
})
export class HomeModule { }
