import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultResultComponent } from './default-result.component';

describe('DefaultResultComponent', () => {
  let component: DefaultResultComponent;
  let fixture: ComponentFixture<DefaultResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DefaultResultComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DefaultResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
