import { Component, OnInit } from '@angular/core';
import { CategoryItem, Releases } from 'src/app/interfaces';
import { AlbumService, CategoryService } from 'src/app/core/http';
import { LoaderService } from 'src/app/core/services/loader.service';
import { ErrorService } from 'src/app/core/services/error.service';

@Component({
  selector: 'app-default-result',
  templateUrl: './default-result.component.html',
  styleUrls: ['./default-result.component.scss']
})
export class DefaultResultComponent implements OnInit {

  public categories!: CategoryItem[];
  public newReleases!: Releases[];

  constructor(
    private categoryService: CategoryService,
    private albumService: AlbumService,
    private loaderService: LoaderService, 
    private errorService : ErrorService,
  ) { }

  ngOnInit(): void {
    this.getCategories();
    this.getNewReleases();
  }

  /**
   * When the getCategories() function is called, show the loader, then call the getCategories()
   * function from the categoryService, and when the response is received, assign it to the categories
   * variable, and when the request is complete, hide the loader.
   */
  getCategories() {
    this.loaderService.show();
    this.categoryService.getCategories().subscribe({
      next: response => this.categories = response,
      error: error => this.errorService.redirectToError401(error),
      complete: () => this.loaderService.hidden()
    })
  }

  /**
   * It takes the response from the API, maps it to a new object, and assigns it to the newReleases
   * property.
   */
  getNewReleases() {
    this.loaderService.show();
    this.albumService.getNewReleases().subscribe({
      next: response => this.newReleases = response.map(({ external_urls, images, name, type, id }) => {
        return { 
          external_url: external_urls.spotify,
          url: external_urls.spotify,
          images: images[1], 
          name, 
          type, 
          id 
        }
      }),
      error: error => this.errorService.redirectToError401(error),
      complete: () => this.loaderService.hidden()
    })
  }
}