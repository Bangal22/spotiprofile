import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { debounceTime, switchMap, Subject, distinctUntilChanged } from 'rxjs';
import { SearchService } from 'src/app/core/http';
import { ArtistService } from 'src/app/core/services';
import { ErrorService } from 'src/app/core/services/error.service';
import { AlbumElement, ArtistsItem, SearchItems, TracksItem } from 'src/app/interfaces/Search';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent implements OnInit {

  @Input() searchForm!: FormGroup<{ search: FormControl<string | null>; }>;

  public searchItems!: SearchItems;
  public search$ = new Subject();
  public inputValue! : boolean;

  constructor(
    private searchService: SearchService,
    private artistService: ArtistService, 
    private errorService : ErrorService,
  ) { }

  ngOnInit(): void {
    this.searchForm.valueChanges.subscribe(value => {
      const str = value.search;
      this.inputValue = !/^\s*$/.test(str ||'');
      if(this.inputValue) this.search$.next(str)
    });
    this.getArtist();
  }

  /**
   * The function getArtist() is a function that subscribes to the filterQueries() function, which
   * returns an observable, and then maps the results of the observable to the searchItems object,
   * which is then used to display the results of the search.
   */
  getArtist() {
    this.filterQueries().subscribe({
      next: ({ albums, artists, tracks }) => {
        this.searchItems = {
          tracks: tracks.items.map(this.getTracksItems).slice(0, 5),
          artist: artists.items.map(this.getArtistItems).filter(artist => artist.images),
          albums: albums.items.map(this.getAlbumItems),
        }
      },
      error: error =>  this.errorService.redirectToError401(error),
    })
  }

  /**
   * When the user types in the search box, wait 400ms, then if the value is different from the last
   * value, then get the search values from the search service.
   * @returns The searchService.getSearchValues(value) is returning an Observable.
   */
  filterQueries() {
    return this.search$.pipe(
      debounceTime(400),
      distinctUntilChanged(),
      switchMap(value => {
        if (!value) value = this.artistService.gerRandomArtist();
        return this.searchService.getSearchValues(value);
      })
    )
  }

  /**
   * It takes an object with the properties name, type, id, images, and external_urls and returns an
   * object with the properties name, type, id, images, and url.
   * @param {ArtistsItem}  - ArtistsItem
   * @returns An object with the following properties: name, type, id, images, and url.
   */
  getArtistItems({ name, type, id, images, external_urls }: ArtistsItem) {
    return {
      name,
      type,
      id,
      images: images[1],
      url: `#/artists/${id}`, 
      external_url: external_urls.spotify,
    }
  }

  /**
   * It takes an object with the properties name, type, id, images, and external_urls, and returns an
   * object with the properties name, type, id, images, and url
   * @param {AlbumElement}  - name, type, id, images, external_urls
   * @returns An array of objects with the following properties:
   * name, type, id, images, url
   */
  getAlbumItems({ name, type, id, images, external_urls }: AlbumElement) {
    return {
      name,
      type,
      id,
      images: images[1],
      external_url: external_urls.spotify,
      url: external_urls.spotify,
    }
  }

  /**
   * It takes an object with the properties id, name, artists, album, duration_ms, and external_urls,
   * and returns an object with the properties id, name, artists, images, duration_ms, album_name, and
   * url
   * @param {TracksItem}  - id,
   * @returns An object with the following properties:
   * id, name, artists, images, duration_ms, album_name, url
   */
  getTracksItems({ id, name, artists, album, duration_ms, external_urls }: TracksItem) {
    return {
      id,
      name,
      artists,
      images: album.images[2],
      duration_ms,
      album_name: album.name,
      url: external_urls.spotify
    }
  }

}
