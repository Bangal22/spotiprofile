import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchRoutingModule } from './search-routing.module';
import { SearchComponent } from './pages/search/search.component';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { SearchResultComponent } from './components/search-result/search-result.component';
import { DefaultResultComponent } from './components/default-result/default-result.component';

@NgModule({
  declarations: [
    SearchComponent,
    SearchResultComponent,
    DefaultResultComponent
  ],
  imports: [
    CommonModule,
    SearchRoutingModule,
    FormsModule, 
    ReactiveFormsModule, 
    SharedModule
  ]
})
export class SearchModule { }
