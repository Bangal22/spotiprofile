import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Artist } from 'src/app/interfaces';
import { ArtistService } from 'src/app/core/http';
import { LoaderService } from 'src/app/core/services/loader.service';
import { ErrorService } from 'src/app/core/services/error.service';

@Component({
  selector: 'app-artist-info',
  templateUrl: './artist-info.component.html',
  styleUrls: ['./artist-info.component.scss']
})
export class ArtistInfoComponent implements OnInit {
  public artist!: Artist;

  constructor(
    private artistService: ArtistService,
    private activatedRoute: ActivatedRoute,
    private loaderService: LoaderService, 
    private errorService : ErrorService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(({ params }: any) => {
      this.getArtistById(params.id)
    })
  }

  /**
   * The function getArtistById() takes an id as a parameter, shows the loader, calls the
   * getArtistById() function from the artistService, and then hides the loader
   * @param {string} id - string - the id of the artist
   */
  getArtistById(id: string) {
    this.loaderService.show();
    this.artistService.getArtistById(id).subscribe({
      next: (response) => this.artist = response,
      error: error => this.errorService.redirectToError401(error),
      complete: () => this.loaderService.hidden()
    })
  }
}
