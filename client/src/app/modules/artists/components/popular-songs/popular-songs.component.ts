import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlbumReduced, TrackReduced } from 'src/app/interfaces';
import { ArtistService } from 'src/app/core/http';
import { LoaderService } from 'src/app/core/services/loader.service';
import { ErrorService } from 'src/app/core/services/error.service';

@Component({
  selector: 'app-popular-songs',
  templateUrl: './popular-songs.component.html',
  styleUrls: ['./popular-songs.component.scss']
})
export class PopularSongsComponent implements OnInit {
  public tracks!: TrackReduced[]
  public albums!: AlbumReduced[]

  constructor(
    private artistService: ArtistService,
    private activatedRoute: ActivatedRoute,
    private loaderService: LoaderService,
    private errorService : ErrorService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(({ params }: any) => {
      this.getArtistTopTracks(params.id);
    })
  }

  /**
   * It takes an id, calls the getArtistTopTracks method from the artistService, and then maps the
   * response to a new object
   * @param {string} id - string - the id of the artist
   */
  getArtistTopTracks(id: string) {
    this.loaderService.show();
    this.artistService.getArtistTopTracks(id).subscribe({
      next: response => this.tracks = response.map(({ id, name, artists, album, duration_ms, external_urls }): TrackReduced => {
        return {
          id,
          name,
          artists,
          images: album.images[2],
          duration_ms,
          album_name: album.name,
          url: external_urls.spotify
        }
      }),
      error: error => this.errorService.redirectToError401(error),
      complete: () => this.loaderService.hidden()
    })
  }

}
