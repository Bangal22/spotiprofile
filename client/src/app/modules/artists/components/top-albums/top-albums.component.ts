import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlbumReduced } from 'src/app/interfaces';
import { AlbumService } from 'src/app/core/http';
import { LoaderService } from 'src/app/core/services/loader.service';
import { ErrorService } from 'src/app/core/services/error.service';

@Component({
  selector: 'app-top-albums',
  templateUrl: './top-albums.component.html',
  styleUrls: ['./top-albums.component.scss']
})
export class TopAlbumsComponent implements OnInit {

  @Input() albums!: AlbumReduced[]

  constructor(
    private activatedRoute: ActivatedRoute,
    private albumService: AlbumService,
    private loaderService: LoaderService, 
    private errorService : ErrorService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(({ params }: any) => {
      this.getAlbums(params.id);
    })
  }

  /**
   * I'm using the Spotify API to get a list of albums from an artist, and then I'm mapping the
   * response to a new object with only the properties I need.
   * @param {string} id - string -&gt; the id of the artist
   */
  getAlbums(id: string) {
    this.loaderService.show();
    this.albumService.getAlbumArtist(id, '6').subscribe({
      next: (response) => this.albums = response.map(({ external_urls, images, name, type }): AlbumReduced => {
        return {
          external_url: external_urls.spotify,
          images: images[1],
          name,
          type,
          url : external_urls.spotify,
        }
      }),
      error: error => this.errorService.redirectToError401(error),
      complete: () => this.loaderService.hidden()
    })
  }

}
