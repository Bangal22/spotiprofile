import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArtistsRoutingModule } from './artists-routing.module';
import { ArtistsComponent } from './pages/artists/artists.component';
import { ArtistProfileComponent } from './pages/artist-profile/artist-profile.component';
import { ArtistInfoComponent } from './components/artist-info/artist-info.component';
import { PopularSongsComponent } from './components/popular-songs/popular-songs.component';
import { SharedModule } from '../../shared/shared.module';
import { TopAlbumsComponent } from './components/top-albums/top-albums.component';

@NgModule({
  declarations: [
    ArtistsComponent,
    ArtistProfileComponent,
    ArtistInfoComponent,
    PopularSongsComponent,
    TopAlbumsComponent
  ],
  imports: [
    CommonModule,
    ArtistsRoutingModule, 
    SharedModule
  ]
})
export class ArtistsModule { }
