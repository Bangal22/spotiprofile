import { Component, OnInit } from '@angular/core';
import { ArtistService } from 'src/app/core/http';
import { ArtistReduced } from 'src/app/interfaces';
import { LoaderService } from 'src/app/core/services/loader.service';
import { ErrorService } from 'src/app/core/services/error.service';
import { changeTimeRange, setTimeRange } from 'src/app/core/utils';

@Component({
  selector: 'app-artists',
  templateUrl: './artists.component.html',
  styleUrls: ['./artists.component.scss']
})
export class ArtistsComponent implements OnInit {
  public timeRange: string = "Last 4 weeks";
  public artists!: ArtistReduced[];

  constructor(
    private artistService: ArtistService,
    private loaderService: LoaderService, 
    private errorService : ErrorService
  ) { }

  ngOnInit(): void {
    this.getArtist('50', 'short_term', "1");
  }

  /**
   * It takes in a limit, timeRange, and id, and then it calls the getArtist function from the
   * artistService, which returns an observable. 
   * 
   * The observable is then subscribed to, and the next, error, and complete functions are defined. 
   * 
   * The next function takes in a response, and then maps over the response, and returns an object with
   * the name, type, images, id, and url properties. 
   * 
   * The error function takes in an error, and then logs the error to the console. 
   * 
   * The complete function calls the changeRange function, and passes in the id and timeRange.
   * @param {string} limit - string = '5'
   * @param {string} timeRange - short_term, medium_term, long_term
   * @param {string} id - string - the id of the artist
   */
  getArtist(limit: string, timeRange: string, id: string) {
    this.loaderService.show();
    this.artistService.getArtist(limit, timeRange).subscribe({
      next: (response) => this.artists = response.map(({ name, type, images, id, external_urls }): ArtistReduced => {
        return {
          name,
          type,
          images: images[1],
          id,
          external_url: external_urls.spotify,
          url: `#/artists/${id}`
        }
      }),
      error: error => this.errorService.redirectToError401(error),
      complete: () => {
        this.changeRange(id, timeRange)
        this.loaderService.hidden()
      },
    })
  }

  /**
   * "changeRange() is a function that takes in an id and a timeRange, and then calls changeTimeRange()
   * and setTimeRange() with those parameters."
   * @param {string} id - the id of the element that was clicked
   * @param {string} timeRange - string - the time range to set the chart to
   */
  changeRange(id: string, timeRange: string) {
    changeTimeRange(id);
    this.timeRange = setTimeRange(timeRange);
  }

}
