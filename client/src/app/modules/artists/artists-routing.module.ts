import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArtistProfileComponent } from './pages/artist-profile/artist-profile.component';
import { ArtistsComponent } from './pages/artists/artists.component';

const routes: Routes = [
  {path: '', component : ArtistsComponent},
  {path: ':id', component : ArtistProfileComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArtistsRoutingModule { }
