import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { RefreshTokenService } from 'src/app/core/http';
import { JwtService } from 'src/app/core/services';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.scss']
})
export class ErrorPageComponent implements OnInit {
  @ViewChild("continueButton") continueButton!: ElementRef;

  public loading = false;

  constructor(
    private refreshTokenService: RefreshTokenService,
    private jwtService: JwtService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  goHome() {
    const refreshToken = this.jwtService.getRefreshToken();
    this.loading = true;
    this.continueButton.nativeElement.disabled = true;
    this.continueButton.nativeElement.style.opacity = "0.6";
    this.refreshTokenService.refreshToken(refreshToken).subscribe({
      next: ({ access_token }) => this.jwtService.updateAccessToken(access_token),
      error: error => console.error(error),
      complete: () => this.router.navigate(['/home'])
    })
  }

}
