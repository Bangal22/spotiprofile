import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JwtService } from 'src/app/core/services';
import { getHashParams } from 'src/app/core/utils';

@Component({
  selector: 'app-validation',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.scss']
})
export class ValidationComponent implements OnInit {

  constructor(
    private jwkService : JwtService, 
    private router : Router
  ) { }

  ngOnInit(): void {
    const { access_token, refresh_token, error } = getHashParams();
    if (!error){
      this.jwkService.saveTokens({
        access_token, 
        refresh_token
      })
      this.router.navigate(['/'])
    }
  }

}
