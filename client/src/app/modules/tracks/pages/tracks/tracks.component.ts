import { Component, OnInit } from '@angular/core';
import { TracksService } from 'src/app/core/http';
import { ColorService } from 'src/app/core/services';
import { ErrorService } from 'src/app/core/services/error.service';
import { LoaderService } from 'src/app/core/services/loader.service';
import { changeTimeRange, setTimeRange } from 'src/app/core/utils';

@Component({
  selector: 'app-tracks',
  templateUrl: './tracks.component.html',
  styleUrls: ['./tracks.component.scss']
})
export class TracksComponent implements OnInit {

  public tracks!: any;
  public timeRange: string = "Last 4 weeks";
  public bgColor!: string;

  constructor(
    private colorService: ColorService,
    private tracksService: TracksService,
    private loaderService: LoaderService,
    private errorService : ErrorService,
  ) { }

  ngOnInit(): void {
    this.bgColor = this.colorService.gerRandomColor()
    this.getTracks('50', 'short_term', '1');
  }

  /**
   * It gets tracks from the Spotify API, and then changes the range of the tracks.
   * @param {string} limit - number of tracks to be returned
   * @param {string} timeRange - string = 'short_term'
   * @param {string} id - the id of the button that was clicked
   */
  getTracks(limit: string, timeRange: string, id: string) {
    this.loaderService.show();
    this.tracksService.getTracks(limit, timeRange).subscribe({
      next: (response) => this.tracks = response.map(({ id, name, artists, album, external_urls, duration_ms }) => {
        return {
          id,
          name,
          artists,
          images: album.images[2],
          url: external_urls.spotify,
          duration_ms,
          album_name: album.name
        }
      }),
      error: error => this.errorService.redirectToError401(error),
      complete: () => {
        this.changeRange(id, timeRange)
        this.loaderService.hidden()
      },
    })
  }

  /**
   * "changeRange() is a function that takes in an id and a timeRange, and then calls changeTimeRange()
   * and setTimeRange() with those parameters."
   * @param {string} id - the id of the element that was clicked
   * @param {string} timeRange - string - the time range to set the chart to
   */
  changeRange(id: string, timeRange: string) {
    changeTimeRange(id);
    this.timeRange = setTimeRange(timeRange);
  }
}
