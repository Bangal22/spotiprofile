export const environment = {
  production: true, 
  API_URL : 'https://spotilife-api.onrender.com', 
  API_SPOTIFY : 'https://api.spotify.com/v1',
};
